import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-book-exam-pop-up',
  templateUrl: './book-exam-pop-up.component.html',
  styleUrls: ['./book-exam-pop-up.component.css']
})
export class BookExamPopUpComponent implements OnInit {

  dataIs: any;
  StartDateArr: any = [];
  EndDateArr: any = [];
  AvailableDates: any = [];
  count: any = 0;
  DateIs: any;
  flag: any = 0;
  SelectedDate: any;
  CourseData: any = [];
  SeatAvailable: any = 0;
  ExamSlotData: any = [];
  Data: any = [];
  ExamStartSlot: any = [];
  ExamEndSlot: any = [];

  TempExamStartSlot: any = [];
  TempExamEndSlot: any = [];

  NExamStartSlot: any = [];
  NExamEndSlot: any = [];
  holidaysData: any = [];
  flag_1: any;
  CourseID: any;
  data: any;
  E_StartTime: any;
  E_EndTime: any;
  LS_StartTime: any;
  LS_EndTime: any;
  ES_StartTime: any;//Exam Start Time From Server
  ES_EndTime: any;//Exam Start Time From Server
  url: any;
  urlArray: any;
  CampusName: any;
  ED_StartTime: any;//Delete Exam Start Time From Server
  ED_EndTime: any;//Delete Exam Start Time From Server 
  Seats: any;
  sid: any;
  SelectedCampus: any = "Macomb";
  note;
  checkedNewTime: any;
  startTime_Manual: any;
  endTime_Manual: any;
  MySlot = ['13:15', '13:30', '13:45', '14:00', '14:15', '14:30', '14:45', '15:00', '15:15', '15:30', '15:45', '15:00'];
  ExamTimeSlot: Array<any> = [];
  constructor(private http: HttpClient, private snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<BookExamPopUpComponent>,
    @Inject(MAT_DIALOG_DATA) public impdata: any) {
    this.url = window.location.href;
    // this.urlArray="https://wiu.edu/CITR/ExamSchedule/macomb".split("/");
    this.urlArray = this.url.split("/");
    this.CampusName = this.urlArray[5];
    console.log(this.CampusName);
    console.log(Number("830") > Number("1310"));
    this.CourseID = impdata.cid;
    this.data = impdata;
    this.CourseData = impdata;
    this.sid = impdata.sid;
    this.note = impdata.note;
    console.log(this.note);
    this.ES_StartTime = (impdata.ES_StartTime).split(":");
    this.ES_EndTime = (impdata.ES_EndTime).split(":");
    this.GetExamTime();
    this.GetTotalSeats();

    this.CheckHolidays(impdata.ExamStartDate);
  }


  InititData() {

    let ExamStartDateFull = new Date(this.data.examStartDateFull);
    let ExamEndDateFull = new Date(this.data.examEndDateFull);
    this.AvailableDates.length = 0;
    let Today = new Date();
    ExamStartDateFull.setHours(this.ES_StartTime[0]);
    ExamStartDateFull.setMinutes(this.ES_StartTime[1]);
    ExamEndDateFull.setHours(this.ES_EndTime[0]);
    ExamEndDateFull.setMinutes(this.ES_EndTime[1]);
    console.log(ExamStartDateFull);
    console.log(ExamEndDateFull);

    // this.CheckHoldaysDate();
    let tempStartDate = ExamStartDateFull.getFullYear() + "/" + ExamStartDateFull.getMonth() + "/" + ExamStartDateFull.getDate();
    let tempEndDate = ExamEndDateFull.getFullYear() + "/" + ExamEndDateFull.getMonth() + "/" + ExamEndDateFull.getDate();
    let TodayIs = Today.getFullYear() + "/" + Today.getMonth() + "/" + Today.getDate()
    //  if(tempStartDate==TodayIs){
    //   let ExamStartDateFullMonth=Number(ExamStartDateFull.getMonth())+1;
    //   this.AvailableDates[this.count]=ExamStartDateFull.getFullYear()+"/"+ExamStartDateFullMonth+"/"+ExamStartDateFull.getDate();
    //   this.count++;

    //  }
    //  if(tempEndDate==TodayIs){
    //   let ExamEndDateFullMonth=Number(ExamEndDateFull.getMonth())+1;
    //   this.AvailableDates[this.count]=ExamEndDateFull.getFullYear()+"/"+ExamEndDateFullMonth+"/"+ExamEndDateFull.getDate();
    //   this.count++;

    //  }
    while (ExamStartDateFull <= ExamEndDateFull) {

      if (this.CheckDay(ExamStartDateFull) == 1 && this.CheckHoldaysDate(ExamStartDateFull) == 0) {

        let ExamStartDateFullMonth = Number(ExamStartDateFull.getMonth()) + 1;
        this.AvailableDates[this.count] = ExamStartDateFull.getFullYear() + "/" + ExamStartDateFullMonth + "/" + ExamStartDateFull.getDate();

        this.count++;

        // }
      }
      ExamStartDateFull.setDate(ExamStartDateFull.getDate() + 1);
    }



    let startSlotTime = new Date(this.data.examStartDateFull);
    let endSlotTime = new Date(this.data.examStartDateFull);

    startSlotTime.setHours(this.ES_StartTime[0]);
    startSlotTime.setMinutes(this.ES_StartTime[1]);

    endSlotTime.setHours(this.ES_EndTime[0]);
    endSlotTime.setMinutes(this.ES_EndTime[1]);
    let startSlotTime_Next = new Date(startSlotTime);
    let startTimeSlotMin = Number(startSlotTime.getMinutes());
    let Num = startTimeSlotMin + Number(this.data.ExamTime)
    startSlotTime_Next.setMinutes(Num)


    while (startSlotTime_Next <= endSlotTime) {
      //    console.log(Number(startSlotTime_Next.getHours())+"--"+Number(startSlotTime_Next.getHours()));
      // this.ExamTimeSlot.push({StartTime:ExamStartDateFull.getHours()+":"+ExamStartDateFull.getMinutes(),EndTime:NewEndTime.getHours()+":"+NewEndTime.getMinutes()});
      if (Number(startSlotTime.getHours()) != 12) {
        if (Number(startSlotTime_Next.getHours()) != 12) {
          this.ExamStartSlot[this.flag] = String("0" + startSlotTime.getHours()).slice(-2) + ":" + String("0" + startSlotTime.getMinutes()).slice(-2);
          this.ExamEndSlot[this.flag] = String("0" + startSlotTime_Next.getHours()).slice(-2) + ":" + String("0" + startSlotTime_Next.getMinutes()).slice(-2);

          this.flag++;
        }
      }
      startSlotTime_Next.setMinutes(startSlotTime_Next.getMinutes() + 15);
      startSlotTime.setMinutes(startSlotTime.getMinutes() + 15);
    }



  }
  GetLunchTime() {
    this.http.get("http://wiu.edu/CITR/MathTutoring/getSaveLunchTime.sphp?read=true")
      .subscribe((data) => {
        //this.Oldseats=data;
        //    console.log(data);
        this.LS_StartTime = data[0].EndTime;
        this.LS_StartTime = data[0].StartTime
        // this.L_EndTime=this.LS_StartTime;
        // this.L_StartTime=this.LS_StartTime;
        // console.log("StartTime:"+this.ES_StartTime[0]);
        // this.openSnackBar("SuccessFully Updated","Ok");
      }, (error) => {

        console.log(error);

      });
  }
  GetExamTime() {
    this.http.get("http://wiu.edu/CITR/MathTutoring/getSaveExamTime.sphp?read=true&ExamDate=All&campus=" + this.CampusName)
      .subscribe((data) => {
        //this.Oldseats=data;
        //    console.log(data);
        this.ES_EndTime = (data[0].EndTime).split(":");
        this.ES_StartTime = (data[0].StartTime).split(":");
        console.log("AllTime:" + this.ES_StartTime);
        // console.log("StartTime:"+this.ES_StartTime[0]);
        // this.openSnackBar("SuccessFully Updated","Ok");
      }, (error) => {

        console.log(error);

      });
  }
  GetExamTime_1() {
    this.http.get("http://wiu.edu/CITR/MathTutoring/getSaveExamTime.sphp?read=true&ExamDate=All&campus=" + this.CampusName)
      .subscribe((data) => {

        this.ES_EndTime = (data[0].EndTime).split(":");
        this.ES_StartTime = (data[0].StartTime).split(":");
        console.log("AllTime:" + this.ES_StartTime);
        this.MakeSlot1(this.ES_StartTime, this.ES_EndTime);
        // console.log("StartTime:"+this.ES_StartTime[0]);
        // this.openSnackBar("SuccessFully Updated","Ok");
      }, (error) => {

        console.log(error);

      });
  }
  CheckDay(ExamStartDateFull: any) {
    ExamStartDateFull = new Date(ExamStartDateFull);
    // console.log("---->"+ExamStartDateFull.setHours(0,0,0,0));
    // console.log("..."+new Date().setHours(0,0,0,0))
    let temp = String(ExamStartDateFull);
    let temparr = temp.split(" ", 2);
    // console.log(temparr[0]);
    let Today = new Date();
    //    console.log("today-"+Today);
    Today.setHours(this.ES_StartTime[0]);
    Today.setMinutes(this.ES_StartTime[1]);

    if (temparr[0] == "Sun" || temparr[0] == "Sat") {

      return 0;
    }
    else {
      if (ExamStartDateFull.setHours(0, 0, 0, 0) >= new Date().setHours(0, 0, 0, 0)) {
        return 1;
      }
      else {
        return 0;
      }
    }

  }



  CheckHolidays(CheckDate: any) {
    // let ExamStartDateFullMonth=Number(ExamStartDateFull.getMonth())+1;
    // let TempDate=ExamStartDateFull.getFullYear()+"/"+ExamStartDateFullMonth+"/"+ExamStartDateFull.getDate();
    this.http.get("http://wiu.edu/CITR/MathTutoring/getHolidaySchedule.sphp?examDate=" + CheckDate + "&campus=" + this.CampusName)
      .subscribe((dataIs) => {

        this.holidaysData = dataIs;
        //  console.log(this.holidaysData);
        // this.GetExamTime();
        this.InititData();
        // this.CheckHoldaysDate();
      }, (error) => {

        console.log(error);

      });
    //console.log(this.flag_1);

  }
  CheckHoldaysDate(ExamStartDateFull: any) {
    //   console.log(this.holidaysData);
    let ExamStartDateFullMonth = Number(ExamStartDateFull.getMonth()) + 1;
    let temp = 0;
    let TempDate = ExamStartDateFull.getFullYear() + "/" + ExamStartDateFullMonth + "/" + ExamStartDateFull.getDate();
    for (let i = 0; i < this.holidaysData.length; i++) {
      //    console.log(this.holidaysData[i].examDate+"=="+TempDate);
      if (this.holidaysData[i].examDate == TempDate) {
        temp = 1;
        break
      }

    }
    return temp;
  }

  getBgColor(i: any) {
    // console.log(i);
    if (i % 2 == 0) {
      return '#eaeaea';
    }
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  CancelBtn() {
    this.dialogRef.close();
  }
  SaveExamDate(selectedDate: any) {
    //   console.log(this.CourseData.course_title);
    this.dialogRef.close({ selectedDate: selectedDate, c_title: this.CourseData.course_title, courseStar: this.CourseData.course_star, courseShort: this.CourseData.courseShort });
  }
  MakeSlot1(StartTime, EndTime) {


    let startSlotTime = new Date(this.data.examStartDateFull);
    let endSlotTime = new Date(this.data.examStartDateFull);

    startSlotTime.setHours(StartTime[0]);
    startSlotTime.setMinutes(StartTime[1]);

    endSlotTime.setHours(EndTime[0]);
    endSlotTime.setMinutes(EndTime[1]);
    let startSlotTime_Next = new Date(startSlotTime);
    let startTimeSlotMin = Number(startSlotTime.getMinutes());
    let Num = startTimeSlotMin + Number(this.data.ExamTime)
    startSlotTime_Next.setMinutes(Num)
    console.log(startSlotTime_Next + "===" + endSlotTime);
    this.flag = 0;
    while (startSlotTime_Next <= endSlotTime) {
      console.log("In While Loop");
      // this.ExamTimeSlot.push({StartTime:ExamStartDateFull.getHours()+":"+ExamStartDateFull.getMinutes(),EndTime:NewEndTime.getHours()+":"+NewEndTime.getMinutes()});
      if (Number(startSlotTime.getHours()) != 12) {
        if (Number(startSlotTime_Next.getHours()) != 12) {
          this.ExamStartSlot[this.flag] = String("0" + startSlotTime.getHours()).slice(-2) + ":" + String("0" + startSlotTime.getMinutes()).slice(-2);
          this.ExamEndSlot[this.flag] = String("0" + startSlotTime_Next.getHours()).slice(-2) + ":" + String("0" + startSlotTime_Next.getMinutes()).slice(-2);
          console.log(String("0" + startSlotTime.getHours()).slice(-2) + ":" + String("0" + startSlotTime.getMinutes()).slice(-2));
          this.flag++;
        }
      }
      startSlotTime_Next.setMinutes(startSlotTime_Next.getMinutes() + 15);
      startSlotTime.setMinutes(startSlotTime.getMinutes() + 15);
    }
  }
  MakeSlot(StartTime, EndTime) {

    console.log(StartTime, EndTime);
    let startSlotTime = new Date(this.data.examStartDateFull);
    let endSlotTime = new Date(this.data.examStartDateFull);

    startSlotTime.setHours(StartTime[0]);
    startSlotTime.setMinutes(StartTime[1]);

    endSlotTime.setHours(EndTime[0]);
    endSlotTime.setMinutes(EndTime[1]);
    console.log(startSlotTime + "===" + endSlotTime);

    let startSlotTime_Next = new Date(startSlotTime);
    let startTimeSlotMin = Number(startSlotTime.getMinutes());
    let Num = startTimeSlotMin + Number(this.data.ExamTime)
    startSlotTime_Next.setMinutes(Num)
    console.log(startSlotTime_Next + "===" + endSlotTime);
    this.flag = 0;
    while (startSlotTime <= endSlotTime) {
      console.log("In While Loop");
      // this.ExamTimeSlot.push({StartTime:ExamStartDateFull.getHours()+":"+ExamStartDateFull.getMinutes(),EndTime:NewEndTime.getHours()+":"+NewEndTime.getMinutes()});
      if (Number(startSlotTime.getHours()) != 12) {
        if (Number(startSlotTime_Next.getHours()) != 12) {
          this.NExamStartSlot[this.flag] = String("0" + startSlotTime.getHours()).slice(-2) + ":" + String("0" + startSlotTime.getMinutes()).slice(-2);
          this.NExamEndSlot[this.flag] = String("0" + startSlotTime_Next.getHours()).slice(-2) + ":" + String("0" + startSlotTime_Next.getMinutes()).slice(-2);
          console.log(this.NExamStartSlot);
          this.flag++;
        }
      }
      startSlotTime_Next.setMinutes(startSlotTime_Next.getMinutes() + 15);
      startSlotTime.setMinutes(startSlotTime.getMinutes() + 15);
    }

  }
  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
  CheckSeatAvailable(selectedDate: any, ) {
    //  console.log(this.SelectedCampus);

    if (this.SelectedCampus == undefined) {
      console.log("Select Campus");
      this.openSnackBar("Please Select Exam Campus", "Ok");
    }
    else {
      this.GetTotalSeats();
      this.SeatAvailable = 1;
      this.ExamTimeSlot.length = 0;

      this.NExamStartSlot.length = 0;
      if (selectedDate == '') {
        selectedDate = 'ALL';
      }
      console.log("http://wiu.edu/CITR/MathTutoring/getSaveExamTime.sphp?read=true&ExamDate=" + selectedDate + "&campus=" + this.SelectedCampus);
      this.http.get("http://wiu.edu/CITR/MathTutoring/getSaveExamTime.sphp?read=true&ExamDate=" + selectedDate + "&campus=" + this.SelectedCampus)
        .subscribe((data) => {
          //this.Oldseats=data;
          console.log(data);
          if (data != '') {
            console.log(data);
            this.ES_EndTime = (data[0].EndTime).split(":");
            this.ES_StartTime = (data[0].StartTime).split(":");

            this.MakeSlot(this.ES_StartTime, this.ES_EndTime);
            console.log(this.ES_StartTime);
            console.log("In If");
          }
          else {
            //  this.InititData();
            this.ES_EndTime = 0;
            this.ES_StartTime = 0;
            console.log("In Else");
            this.GetExamTime_1();

          }

        }, (error) => {

          console.log(error);

        });


      this.http.get("http://wiu.edu/CITR/MathTutoring/GetSlot.php?examDate=" + selectedDate + "&campus=" + this.SelectedCampus)
        .subscribe((data) => {
          //   console.log(data);
          this.Data = data;

          // this.TimeData(data);
          console.log(this.Data);
          this.setTimeSlotData(data);
          //  console.log(this.ExamTimeSlot);
        }, (error) => {

          console.log(error);

        });
    }
  }

  setTimeSlotData(result) {
    let flag = 0;
    let MaxSeatBooked = 0;
    // // console.log(result); 
    //  console.log(this.ExamStartSlot);
    for (let i = 0; i < this.ExamStartSlot.length; i++) {
      flag = 0;
      MaxSeatBooked = 0;

      // console.log(result[flag].col.replace(":","")+"---"+"---"+this.ExamStartSlot[i].replace(new RegExp('^0+(?!$)',''),'').replace(":","")+"---"+this.ExamEndSlot[i].replace(new RegExp('^0+(?!$)',''),'').replace(":",""));
      while (Number(result[flag].col.replace(":", "")) < Number(this.ExamEndSlot[i].replace(new RegExp('^0+(?!$)', ''), '').replace(":", ""))) {
        // console.log("In While");
        Number(result[flag].col.replace(":", "")) + "---" + Number(this.ExamEndSlot[i].replace(new RegExp('^0+(?!$)', ''), '').replace(":", ""))
        if (Number(result[flag].col.replace(":", "")) > Number(this.ExamStartSlot[i].replace(new RegExp('^0+(?!$)', ''), '').replace(":", ""))) {

          if (MaxSeatBooked < Number(result[flag].A)) {
            MaxSeatBooked = Number(result[flag].A);
            console.log(MaxSeatBooked);
          }
        }
        flag++;
      }
      flag++;
      console.log(this.ExamStartSlot[i] + "--" + this.ExamEndSlot[i] + "=" + MaxSeatBooked);
      let SlotAval = Number(this.Seats) - MaxSeatBooked;
      //   console.log(SlotAval+"---"+this.Seats);
      if (SlotAval < 0) {
        SlotAval = 0;
      }
      this.ExamTimeSlot.push({ StartTime: this.ExamStartSlot[i], EndTime: this.ExamEndSlot[i], Seat: SlotAval });


    }
    //     MaxSeatBooked=0;

    // for(let i=0;i<this.ExamStartSlot.length;i++)
    // {
    //  flag=0;
    //   MaxSeatBooked=0;


    //   let regex = new RegExp(':', 'g');

    //   while(parseInt(result[flag].col .replace(regex, ""),10)<parseInt(this.ExamEndSlot[i].replace(new RegExp('^0+(?!$)',''),'') .replace(regex, ""),10))
    //   {
    //     flag++;
    //     if(parseInt(result[flag].col .replace(regex, ""),10)>=parseInt(this.ExamStartSlot[i].replace(new RegExp('^0+(?!$)',''),'') .replace(regex, ""),10))
    //     {

    //         if(MaxSeatBooked<Number(result[flag].A))
    //         {
    //           MaxSeatBooked=Number(result[flag].A);
    //           console.log(MaxSeatBooked);
    //         }
    //      }


    //     console.log(parseInt(result[flag].col .replace(regex, ""),10)<parseInt(this.ExamEndSlot[i].replace(new RegExp('^0+(?!$)',''),'') .replace(regex, ""),10));
    //     console.log(parseInt(result[flag].col .replace(regex, ""),10)+"==="+parseInt(this.ExamEndSlot[i].replace(new RegExp('^0+(?!$)',''),'') .replace(regex, ""),10));

    //   }

    //   let SlotAval=Number(this.Seats)-MaxSeatBooked;
    // //   console.log(SlotAval+"---"+this.Seats);
    //   if(SlotAval<0)
    //   {
    //     SlotAval=0;
    //   }
    //   this.ExamTimeSlot.push({StartTime:this.ExamStartSlot[i],EndTime:this.ExamEndSlot[i],Seat:SlotAval});


    // }




    //let MaxSeatBooked=0;
    // console.log("ExamStartSlot:"+this.ExamStartSlot);
    //   for(let i=0;i<this.ExamStartSlot.length;i++)
    //   {
    //     flag=0;
    //     MaxSeatBooked=0;
    //     // while(parseInt(result[flag].col .replace(regExp, "$1$2$3"))<parseInt(this.ExamEndSlot[i].replace(new RegExp('^0+(?!$)',''),'') .replace(regExp, "$1$2$3")))
    //     // {
    //     //   console.log(parseInt(result[flag].col .replace(regExp, "$1$2$3"))+"---"+parseInt(this.ExamEndSlot[i].replace(new RegExp('^0+(?!$)',''),'') .replace(regExp, "$1$2$3")));
    //     //   flag++;
    //     //   if(parseInt(result[flag].col .replace(regExp, "$1$2$3"))>parseInt(this.ExamStartSlot[i].replace(new RegExp('^0+(?!$)',''),'') .replace(regExp, "$1$2$3")))
    //     //   {
    //     //     //console.log(result[flag].col+"---"+this.ExamEndSlot[i]);
    //     //       if(MaxSeatBooked<Number(result[flag].A))
    //     //       {
    //     //         MaxSeatBooked=Number(result[flag].A);
    //     //       }
    //     //    }
    //     // }
    //     //console.log(new Date('25/09/2019 '+result[flag].col));
    //     // while(Date.parse('25/09/2019 '+result[flag].col)<Date.parse('25/09/2019 '+this.ExamEndSlot[i].replace(new RegExp('^0+(?!$)',''),'')))
    //     // {
    //     //   console.log(parseInt(result[flag].col .replace(regExp, "$1$2$3"))+"---"+parseInt(this.ExamEndSlot[i].replace(new RegExp('^0+(?!$)',''),'') .replace(regExp, "$1$2$3")));
    //     //   flag++;
    //     //   if(parseInt(result[flag].col .replace(regExp, "$1$2$3"))>parseInt(this.ExamStartSlot[i].replace(new RegExp('^0+(?!$)',''),'') .replace(regExp, "$1$2$3")))
    //     //   {
    //     //     //console.log(result[flag].col+"---"+this.ExamEndSlot[i]);
    //     //       if(MaxSeatBooked<Number(result[flag].A))
    //     //       {
    //     //         MaxSeatBooked=Number(result[flag].A);
    //     //       }
    //     //    }
    //     // }
    //   //  console.log(this.ExamStartSlot[i]+"--"+this.ExamEndSlot[i]+"="+MaxSeatBooked);
    //     let SlotAval=Number(this.Seats)-MaxSeatBooked;
    //  //   console.log(SlotAval+"---"+this.Seats);
    //     if(SlotAval<0)
    //     {
    //       SlotAval=0;
    //     }
    //   //  this.ExamTimeSlot.push({StartTime:this.ExamStartSlot[i],EndTime:this.ExamEndSlot[i],Seat:SlotAval});


    //   }
    //   let tempFlag=0;




  }

  CheckSlot(StartTime, EndTime) {
    let StartTimeArr = StartTime.split(":");
    let endTimeArr = EndTime.split(":");
    //console.log(StartTimeArr+"---"+endTimeArr);
    if (StartTimeArr[0] <= 11 && endTimeArr[0] >= 12) {
      return 0;
    }
    if (StartTime == "13:00" || StartTime == "13:15") {
      return 0;
    }
    for (let i = 0; i < this.NExamStartSlot.length; i++) {
      //   console.log(this.NExamStartSlot[i]+"----"+StartTime+"---"+EndTime);
      if (this.NExamStartSlot[i] == StartTime || this.NExamStartSlot[i] == EndTime) {

        console.log("YES");
        return 0;

      }


    }
    return 1;

  }
  GetTotalSeats() {
    this.http.get("http://wiu.edu/CITR/MathTutoring/SaveSeats.sphp?seats=0&campus=" + this.SelectedCampus)
      .subscribe((data) => {
        this.Seats = data;
        console.log("Total Seats:" + this.Seats);
      }, (error) => {

        console.log(error);

      });
  }

  TimeData(result: any) {
    let flag_one = 0;

    // for(let i=0;i<result.length;i++)
    // { 
    // //  console.log(result);
    //   flag_one=0;

    //   for(let j=flag_one;j<this.ExamStartSlot.length;j++)
    //   {

    //   if(this.ExamStartSlot[j]==result[i].StartTime||this.ExamEndSlot[j]==result[i].EndTime)
    //   {
    //    this.ExamTimeSlot.push({StartTime:this.ExamStartSlot[j],EndTime:this.ExamEndSlot[j],Seat:result[i].Seats});
    //    flag_one++;
    //    break;

    //   }
    //   else{
    //   //  console.log(this.ExamStartSlot[j]+"="+result[i].StartTime);
    //    this.ExamTimeSlot.push({StartTime:this.ExamStartSlot[j],EndTime:this.ExamEndSlot[j],Seat:this.Seats});
    //    flag_one++;
    //   }
    // }

    // }
    // for(let i=flag_one;i<this.ExamStartSlot.length;i++)
    // {
    //   this.ExamTimeSlot.push({StartTime:this.ExamStartSlot[i],EndTime:this.ExamEndSlot[i],Seat:this.Seats});
    // }
    for (let i = 0; i < result.length; i++) {
      console.log(result);
      for (let j = flag_one; j < this.ExamStartSlot.length; j++) {
        if (this.ExamStartSlot[j] == result[i].StartTime || this.ExamEndSlot[j] == result[i].EndTime) {

          this.ExamTimeSlot.push({ StartTime: this.ExamStartSlot[j], EndTime: this.ExamEndSlot[j], Seat: result[i].Seats });
          flag_one++;
          break;
        }
        else {
          this.ExamTimeSlot.push({ StartTime: this.ExamStartSlot[j], EndTime: this.ExamEndSlot[j], Seat: this.Seats });
          flag_one++;
        }
        // flag_one++;
      }

    }
    for (let i = flag_one; i < this.ExamStartSlot.length; i++) {
      this.ExamTimeSlot.push({ StartTime: this.ExamStartSlot[i], EndTime: this.ExamEndSlot[i], Seat: this.Seats });
    }

    console.log(this.ExamTimeSlot);

  }

  BookStudentExamSlot(StartTime: any, EndTime: any, selectedDate: any) {
    console.log("http://wiu.edu/CITR/MathTutoring/EnterSlots.php?start_TimeSlot=" + StartTime + "&end_TimeSlot=" + EndTime + "&examDate=" + selectedDate + "&sid=" + this.sid);
    //   this.http.get("http://wiu.edu/CITR/ExamSchedule/EnterSlots.php?start_TimeSlot="+StartTime+"&end_TimeSlot="+EndTime+"&examDate="+selectedDate+"&sid="+this.sid)
    //   .subscribe((data) => {

    //  console.log(data);

    //   }, (error) => {

    //       console.log(error);

    //     });


    this.dialogRef.close({
      selectedDate: selectedDate, c_title: this.CourseData.course_title, courseStar: this.CourseData.course_star,
      courseShort: this.CourseData.courseShort, StartTime: StartTime.replace('AM', "").replace("PM", ""), EndTime: EndTime.replace('AM', "").replace('PM', ""), cid: this.CourseID, campus: this.SelectedCampus
    });


  }

  ManualTimeSave(SelectedDate: any) {
    console.log(this.startTime_Manual + " To " + this.endTime_Manual);
    this.BookStudentExamSlot(this.startTime_Manual, this.endTime_Manual, SelectedDate);
  }

  getSeatColor(seatCount: any) {
    if (seatCount > 10) {
      return "#d1efae";
    }
    if (seatCount <= 10 && seatCount > 5) {
      return "#fff7b1";
    }
    if (seatCount <= 5) {
      return "#ffc7b687";
    }
  }
  ngOnInit() {

  }
}
