import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AmazingTimePickerService } from 'amazing-time-picker';
import { Router } from '@angular/router';
interface order_dates {
  id: number;
  day: number;
  month: number;
  year: number;
  reason: string;
}
interface OnDateExamTimeOrder {
  id: number;
  day: number;
  month: number;
  year: number;
  StartTime: string;
  EndTime: string;
}
@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  holidaysData;
  Picker: any;
  Picker1: any;
  picker: any;
  picker1: any;
  reason: any;
  seats: any;
  Oldseats: any;
  displayData: any = "NA";
  o_datesAre: Array<order_dates> = [];
  onDateExamTimeOrder: Array<OnDateExamTimeOrder> = [];
  ES_EndTime: any;
  ES_StartTime: any;
  E_EndTime: any;
  E_StartTime: any;
  ED_EndTime: any;
  ED_StartTime: any;
  L_StartTime: any;
  L_EndTime: any;
  LS_EndTime: any;
  LS_StartTime: any;
  ExamOnDateTime: any;
  url: any;
  urlArray: any;
  CampusName: any;
  o_date: any = [];
  o_year: any = [];
  o_month: any = [];
  constructor(public http: HttpClient, private _snackBar: MatSnackBar, private atp: AmazingTimePickerService, private router: Router) {
    this.url = window.location.href;
    this.url = "http://wiu.edu/CITR/ExamSchedule/macomb";
    // console.log("https://wiu.edu/CITR/ExamSchedule/macomb".split("/"));

    this.urlArray = this.url.split("/");
    this.CampusName = this.urlArray[5];
    console.log(this.CampusName);
    this.CheckHolidays();
    this.GetExamTime();
    this.GetLunchTime();
    this.GetOnDateExamTime();
    let CurrentTime = new Date();

  }

  ngOnInit() {
    this.SaveSeats();
  }

  HolidayOrderTheDates() {
    for (let i = 0; i < this.holidaysData.length; i++) {
      let parts = (this.holidaysData[i].examDate).split("/");
      let tempData = (this.holidaysData[i].examDate).split("/")
      this.o_date[i] = tempData[1];
      this.o_year[i] = tempData[0];
      this.o_month[i] = tempData[2];

      let date = new Date(parseInt(parts[0]),
        parseInt(parts[1]) - 1, parseInt(parts[2]));
      this.o_date[i] = date;
      this.o_datesAre.push({ id: this.holidaysData[i].id, day: parseInt(tempData[2]), month: parseInt(tempData[1]), year: parseInt(tempData[0]), reason: this.holidaysData[i].reason });


    }

    console.log(this.o_datesAre.sort((a, b) => (a.year > b.year) ? -1 : (a.year === b.year) ? ((a.month > b.month) ? -1 : 1) : 1));

  }
  CheckHolidays() {
    let TodaysDate = new Date();
    let TodaysDateMonth = Number(TodaysDate.getMonth()) + 1;
    let TempDate = TodaysDate.getFullYear() + "/" + TodaysDateMonth + "/" + TodaysDate.getDate();
    this.http.get("http://wiu.edu/CITR/ExamSchedule/getHolidaySchedule.sphp?examDate=" + TempDate + "&campus=" + this.CampusName)
      .subscribe((dataIs) => {

        this.holidaysData = dataIs;
        console.log(this.holidaysData[0]);
        this.HolidayOrderTheDates();
        // this.CheckHoldaysDate();
      }, (error) => {

        console.log(error);

      });
    //console.log(this.flag_1);

  }
  SetDisplayData(temp: any) {
    this.displayData = temp;
  }
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
  saveHolidaysData() {
    if (this.Picker.length != 0 && this.reason.length != 0) {
      let DateIs = new Date(this.Picker)
      let DateMonth = Number(DateIs.getMonth()) + 1;
      let HolidayDate = DateIs.getFullYear() + "/" + DateMonth + "/" + DateIs.getDate();


      this.http.get("http://wiu.edu/CITR/ExamSchedule/SaveHolidaysData.sphp?holidayDate=" + HolidayDate + "&reason=" + this.reason + "&campus=" + this.CampusName)
        .subscribe((dataIs) => {
          this.CheckHolidays();
          this.openSnackBar("SuccessFully Created", "Ok");
        }, (error) => {

          console.log(error);

        });
    }
    else {
      this.openSnackBar("Please Enter All Field", "OK");
    }
  }

  DeleteHoliday(holidayDate: any) {
    this.http.get("http://wiu.edu/CITR/ExamSchedule/SaveHolidaysData.sphp?holidayDate=" + holidayDate + "&delete=true&campus=" + this.CampusName)
      .subscribe((dataIs) => {
        this.CheckHolidays();
        this.openSnackBar("SuccessFully Deleted", "Ok");
      }, (error) => {

        console.log(error);

      });


  }
  SaveSeats() {
    console.log("http://wiu.edu/CITR/ExamSchedule/SaveSeats.sphp?seats=" + this.seats + "&campus=" + this.CampusName);
    this.http.get("http://wiu.edu/CITR/ExamSchedule/SaveSeats.sphp?seats=" + this.seats + "&campus=" + this.CampusName)
      .subscribe((data) => {
        this.Oldseats = data;
        this.openSnackBar("SuccessFully Updated", "Ok");
      }, (error) => {

        console.log(error);

      });
  }

  GetExamTime() {
    console.log("http://wiu.edu/CITR/ExamSchedule/getSaveExamTime.sphp?read=true&ExamDate=ALL&campus=" + this.CampusName);
    this.http.get("http://wiu.edu/CITR/ExamSchedule/getSaveExamTime.sphp?read=true&ExamDate=ALL&campus=" + this.CampusName)
      .subscribe((data) => {
        //this.Oldseats=data;
        //     console.log(data);
        this.ES_EndTime = data[0].EndTime;
        this.ES_StartTime = data[0].StartTime
        this.E_EndTime = this.ES_EndTime;
        this.E_StartTime = this.ES_StartTime;
        console.log("StartTime:" + this.ES_StartTime[0]);
        // this.openSnackBar("SuccessFully Updated","Ok");
      }, (error) => {

        //    console.log(error);

      });
  }
  SaveExamTime() {
    //   console.log("http://wiu.edu/CITR/ExamSchedule/getSaveExamTime.sphp?startTime="+this.E_StartTime+"&endTime="+this.E_EndTime+"&read=false");
    this.http.get("http://wiu.edu/CITR/ExamSchedule/getSaveExamTime.sphp?startTime=" + this.E_StartTime + "&endTime=" + this.E_EndTime + "&read=false&campus=" + this.CampusName)
      .subscribe((data) => {

      }, (error) => {

        console.log(error);

      });
  }
  SaveDateExamTime() {
    console.log(this.Picker);
    let DateIs = new Date(this.Picker)
    let DateMonth = Number(DateIs.getMonth()) + 1;
    let SelectedDate = DateIs.getFullYear() + "/" + DateMonth + "/" + DateIs.getDate();
    this.http.get("http://wiu.edu/CITR/ExamSchedule/getSaveExamTime.sphp?startTime=" + this.ED_StartTime + "&endTime=" + this.ED_EndTime + "&read=falseOnDate&ExamDate=" + SelectedDate + "&campus=" + this.CampusName)
      .subscribe((data) => {
        this.GetOnDateExamTime();
      }, (error) => {

        console.log(error);

      });
  }

  OnDateExamTimeOrderTheDates() {
    for (let i = 0; i < this.ExamOnDateTime.length; i++) {
      let parts = (this.ExamOnDateTime[i].examDate).split("/");
      let tempData = (this.ExamOnDateTime[i].examDate).split("/")
      this.o_date[i] = tempData[1];
      this.o_year[i] = tempData[0];
      this.o_month[i] = tempData[2];

      let date = new Date(parseInt(parts[0]),
        parseInt(parts[1]) - 1, parseInt(parts[2]));
      this.o_date[i] = date;
      console.log(this.o_date[0]);
      this.onDateExamTimeOrder.push({ id: this.ExamOnDateTime[i].id, day: parseInt(tempData[2]), month: parseInt(tempData[1]), year: parseInt(tempData[0]), StartTime: this.ExamOnDateTime[i].StartTime, EndTime: this.ExamOnDateTime[i].EndTime });


    }

    // console.log(this.onDateExamTimeOrder.sort((a,b)=>(a.year>b.year)? -1 :(a.year===b.year)?((a.month>b.month)?-1:1):1));
    console.log(this.onDateExamTimeOrder);
  }
  GetOnDateExamTime() {

    this.http.get("http://wiu.edu/CITR/ExamSchedule/GetOnDateExamTime.sphp?campus=" + this.CampusName)
      .subscribe((dataIs) => {

        this.ExamOnDateTime = dataIs;
        console.log(this.ExamOnDateTime);
        //  this.onDateExamTimeOrder=this.ExamOnDateTime;   
        this.OnDateExamTimeOrderTheDates();

        // this.CheckHoldaysDate();
      }, (error) => {

        console.log(error);

      });
    //console.log(this.flag_1);

  }


  DeleteOnDateExamTime(SelectedDate: any) {
    console.log()
    this.http.get("http://wiu.edu/CITR/ExamSchedule/GetOnDateExamTime.sphp?examDate=" + SelectedDate + "&delete=true&campus=" + this.CampusName)
      .subscribe((dataIs) => {
        this.GetOnDateExamTime();
        this.openSnackBar("SuccessFully Deleted", "Ok");
      }, (error) => {

        console.log(error);

      });


  }
  GetLunchTime() {
    this.http.get("http://wiu.edu/CITR/ExamSchedule/getSaveLunchTime.sphp?read=true")
      .subscribe((data) => {
        //this.Oldseats=data;
        //  console.log(data);
        this.LS_StartTime = data[0].EndTime;
        this.LS_StartTime = data[0].StartTime
        this.L_EndTime = this.LS_StartTime;
        this.L_StartTime = this.LS_StartTime;
        // console.log("StartTime:"+this.ES_StartTime[0]);
        // this.openSnackBar("SuccessFully Updated","Ok");
      }, (error) => {

        console.log(error);

      });
  }
  SaveLunchTime() {
    //   console.log("http://wiu.edu/CITR/ExamSchedule/getSaveLunchTime.sphp?startTime="+this.L_StartTime+"&endTime="+this.L_EndTime+"&read=false");
    this.http.get("http://wiu.edu/CITR/ExamSchedule/getSaveLunchTime.sphp?startTime=" + this.L_StartTime + "&endTime=" + this.L_EndTime + "&read=false")
      .subscribe((data) => {
        //this.Oldseats=data;
        console.log(data);

      }, (error) => {

        console.log(error);

      });
  }
  OpenStartClock() {

    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      //  console.log(time);
      this.E_StartTime = time;
      // this.L_StartTime=time;
    });

  }
  OpenEndClock() {

    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      //  console.log(time);
      this.E_EndTime = time;
      // this.L_EndTime=time;
    });

  }
  OpenOnDateStartClock() {
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      //  console.log(time);
      this.ED_StartTime = time;
      // this.L_StartTime=time;
    });
  }
  OpenOnDateEndClock() {
    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      //  console.log(time);
      this.ED_EndTime = time;
      // this.L_EndTime=time;
    });
  }
  OpenStartLunchClock() {

    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      //   console.log(time);

      this.L_StartTime = time;
    });

  }
  OpenEndLunchClock() {

    const amazingTimePicker = this.atp.open();
    amazingTimePicker.afterClose().subscribe(time => {
      //    console.log(time);

      this.L_EndTime = time;
    });
  }
}
