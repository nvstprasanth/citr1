import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-non-wiustudent-book-slot',
  templateUrl: './non-wiustudent-book-slot.component.html',
  styleUrls: ['./non-wiustudent-book-slot.component.css']
})
export class NonWIUStudentBookSlotComponent implements OnInit {

  ExamTime: any = '30';
  picker_1: any;
  SelectedDate: any;
  ExamStartSlot: any = [];
  ExamEndSlot: any = [];
  holidaysData: any = [];
  flag: any = 0;
  Data: any = [];
  Seats;
  TempExamStartSlot: any = [];
  TempExamEndSlot: any = [];
  SeatAvailable;
  NExamStartSlot: any = [];
  NExamEndSlot: any = [];
  c_name; examType;
  s_name; note;
  s_email; s_phone; s_id;
  ExamTimeSlot: Array<any> = [];
  urlArray: any;
  url: any;
  checkedNewTime: any;
  startTime_Manual: any;
  endTime_Manual: any;
  CampusName: any;
  ES_StartTime: any;//Exam Start Time From Server
  ES_EndTime: any;//Exam Start Time From Server
  constructor(private http: HttpClient, private snackBar: MatSnackBar) {
    this.url = window.location.href;
    //   this.urlArray="https://wiu.edu/CITR/ExamSchedule/macomb".split("/");
    this.urlArray = this.url.split("/");
    this.CampusName = this.urlArray[5];
    this.CampusName = 'macomb';
    console.log(this.CampusName);
    this.GetExamTime();
    this.GetTotalSeats();
  }

  ngOnInit() {
  }

  CheckSeats() {
    this.ExamStartSlot.length = 0;
    this.ExamEndSlot.length = 0;
    let SelectedDate = new Date(this.picker_1);
    let SelectedDateMonth = Number(SelectedDate.getMonth()) + 1;
    this.SelectedDate = SelectedDate.getFullYear() + "/" + SelectedDateMonth + "/" + SelectedDate.getDate();
    this.GetExamTime();
    this.createSlot();
    console.log(this.ExamStartSlot);
    console.log(this.ExamEndSlot);
    this.CheckSeatAvailable();
  }
  createSlot() {
    this.ExamStartSlot.length = 0;
    this.ExamEndSlot.length = 0;
    this.flag = 0;
    while (this.ExamStartSlot.length > 0) {
      this.ExamStartSlot.pop();
    }
    let SelectedStartTime = new Date(this.picker_1);
    let SelectedEndTime = new Date(this.picker_1);
    SelectedStartTime.setHours(this.ES_StartTime[0]);
    SelectedStartTime.setMinutes(this.ES_StartTime[1]);

    SelectedEndTime.setHours(this.ES_EndTime[0]);
    SelectedEndTime.setMinutes(this.ES_EndTime[1]);

    console.log(SelectedStartTime);
    let startSlotTime_Next = new Date(SelectedStartTime);
    let startTimeSlotMin = Number(SelectedStartTime.getMinutes());
    let Num = startTimeSlotMin + Number(this.ExamTime)
    console.log(startTimeSlotMin + ":Min:" + Num);
    startSlotTime_Next.setMinutes(Num)


    while (startSlotTime_Next <= SelectedEndTime) {
      //    console.log(Number(startSlotTime_Next.getHours())+"--"+Number(startSlotTime_Next.getHours()));
      // this.ExamTimeSlot.push({StartTime:ExamStartDateFull.getHours()+":"+ExamStartDateFull.getMinutes(),EndTime:NewEndTime.getHours()+":"+NewEndTime.getMinutes()});
      if (Number(SelectedStartTime.getHours()) != 12) {
        if (Number(startSlotTime_Next.getHours()) != 12) {
          this.ExamStartSlot[this.flag] = String("0" + SelectedStartTime.getHours()).slice(-2) + ":" + String("0" + SelectedStartTime.getMinutes()).slice(-2);
          this.ExamEndSlot[this.flag] = String("0" + startSlotTime_Next.getHours()).slice(-2) + ":" + String("0" + startSlotTime_Next.getMinutes()).slice(-2);

          this.flag++;
        }

      }
      startSlotTime_Next.setMinutes(startSlotTime_Next.getMinutes() + 15);
      SelectedStartTime.setMinutes(SelectedStartTime.getMinutes() + 15);
    }
  }
  GetExamTime() {
    console.log("http://wiu.edu/CITR/ExamSchedule/getSaveExamTime.sphp?read=true&ExamDate=All&campus=" + this.CampusName);
    this.http.get("http://wiu.edu/CITR/ExamSchedule/getSaveExamTime.sphp?read=true&ExamDate=All&campus=" + this.CampusName)
      .subscribe((data) => {

        this.ES_EndTime = (data[0].EndTime).split(":");
        this.ES_StartTime = (data[0].StartTime).split(":");

        console.log("StartTime:" + this.ES_StartTime[0]);
      }, (error) => {

        console.log(error);

      });
  }
  MakeSlot1(StartTime, EndTime) {


    let startSlotTime = new Date(this.picker_1);
    let endSlotTime = new Date(this.picker_1);

    startSlotTime.setHours(StartTime[0]);
    startSlotTime.setMinutes(StartTime[1]);

    endSlotTime.setHours(EndTime[0]);
    endSlotTime.setMinutes(EndTime[1]);
    let startSlotTime_Next = new Date(startSlotTime);
    let startTimeSlotMin = Number(startSlotTime.getMinutes());
    let Num = startTimeSlotMin + Number(this.ExamTime)
    startSlotTime_Next.setMinutes(Num)
    console.log(startSlotTime_Next + "===" + endSlotTime);
    this.flag = 0;
    while (startSlotTime_Next <= endSlotTime) {
      console.log("In While Loop");
      // this.ExamTimeSlot.push({StartTime:ExamStartDateFull.getHours()+":"+ExamStartDateFull.getMinutes(),EndTime:NewEndTime.getHours()+":"+NewEndTime.getMinutes()});
      if (Number(startSlotTime.getHours()) != 12) {
        if (Number(startSlotTime_Next.getHours()) != 12) {
          this.ExamStartSlot[this.flag] = String("0" + startSlotTime.getHours()).slice(-2) + ":" + String("0" + startSlotTime.getMinutes()).slice(-2);
          this.ExamEndSlot[this.flag] = String("0" + startSlotTime_Next.getHours()).slice(-2) + ":" + String("0" + startSlotTime_Next.getMinutes()).slice(-2);

          this.flag++;
        }
      }
      startSlotTime_Next.setMinutes(startSlotTime_Next.getMinutes() + 15);
      startSlotTime.setMinutes(startSlotTime.getMinutes() + 15);
    }
  }
  MakeSlot(StartTime, EndTime) {

    console.log(StartTime, EndTime);
    let startSlotTime = new Date(this.picker_1);
    let endSlotTime = new Date(this.picker_1);

    startSlotTime.setHours(StartTime[0]);
    startSlotTime.setMinutes(StartTime[1]);

    endSlotTime.setHours(EndTime[0]);
    endSlotTime.setMinutes(EndTime[1]);
    console.log(startSlotTime + "===" + endSlotTime);

    let startSlotTime_Next = new Date(startSlotTime);
    let startTimeSlotMin = Number(startSlotTime.getMinutes());
    let Num = startTimeSlotMin + Number(this.ExamTime)
    startSlotTime_Next.setMinutes(Num)
    console.log(startSlotTime_Next + "===" + endSlotTime);
    this.flag = 0;
    while (startSlotTime <= endSlotTime) {
      console.log("In While Loop");
      // this.ExamTimeSlot.push({StartTime:ExamStartDateFull.getHours()+":"+ExamStartDateFull.getMinutes(),EndTime:NewEndTime.getHours()+":"+NewEndTime.getMinutes()});
      if (Number(startSlotTime.getHours()) != 12) {
        if (Number(startSlotTime_Next.getHours()) != 12) {
          this.NExamStartSlot[this.flag] = String("0" + startSlotTime.getHours()).slice(-2) + ":" + String("0" + startSlotTime.getMinutes()).slice(-2);
          this.NExamEndSlot[this.flag] = String("0" + startSlotTime_Next.getHours()).slice(-2) + ":" + String("0" + startSlotTime_Next.getMinutes()).slice(-2);
          console.log(this.NExamStartSlot);
          this.flag++;
        }
      }
      startSlotTime_Next.setMinutes(startSlotTime_Next.getMinutes() + 15);
      startSlotTime.setMinutes(startSlotTime.getMinutes() + 15);
    }

  }
  GetExamTime_1() {
    this.http.get("http://wiu.edu/CITR/ExamSchedule/getSaveExamTime.sphp?read=true&ExamDate=All&campus=" + this.CampusName)
      .subscribe((data) => {

        this.ES_EndTime = (data[0].EndTime).split(":");
        this.ES_StartTime = (data[0].StartTime).split(":");
        console.log("AllTime:" + this.ES_StartTime);
        this.MakeSlot1(this.ES_StartTime, this.ES_EndTime);
        // console.log("StartTime:"+this.ES_StartTime[0]);
        // this.openSnackBar("SuccessFully Updated","Ok");
      }, (error) => {

        console.log(error);

      });
  }
  CheckSeatAvailable() {

    this.SeatAvailable = 1;
    this.ExamTimeSlot.length = 0;

    this.NExamStartSlot.length = 0;
    this.http.get("http://wiu.edu/CITR/ExamSchedule/getSaveExamTime.sphp?read=true&ExamDate=" + this.SelectedDate + "&campus=" + this.CampusName)
      .subscribe((data) => {
        //this.Oldseats=data;
        console.log(data);
        if (data != '') {
          console.log(data);
          this.ES_EndTime = (data[0].EndTime).split(":");
          this.ES_StartTime = (data[0].StartTime).split(":");

          this.MakeSlot(this.ES_StartTime, this.ES_EndTime);
          console.log(this.ES_StartTime);
          console.log("In If");
        }
        else {
          //  this.InititData();
          this.ES_EndTime = 0;
          this.ES_StartTime = 0;
          console.log("In Else");
          this.GetExamTime_1();

        }

      }, (error) => {

        console.log(error);

      });

    console.log("http://wiu.edu/CITR/ExamSchedule/GetSlot.php?examDate=" + this.SelectedDate + "&campus=Macomb");
    this.http.get("http://wiu.edu/CITR/ExamSchedule/GetSlot.php?examDate=" + this.SelectedDate + "&campus=" + this.CampusName)
      .subscribe((data) => {
        //   console.log(data);
        this.Data = data;

        // this.TimeData(data);
        console.log(this.Data);
        this.setTimeSlotData(this.Data);
        //  console.log(this.ExamTimeSlot);
      }, (error) => {

        console.log(error);

      });
  }

  setTimeSlotData(result) {
    // let flag=0;
    // let MaxSeatBooked=0;
    //   for(let i=0;i<this.ExamStartSlot.length;i++)
    //   {
    //     flag=0;
    //     MaxSeatBooked=0;
    //     while(result[flag].col<this.ExamEndSlot[i])
    //     {

    //       flag++;
    //       if(result[flag].col>=this.ExamStartSlot[i])
    //       {console.log(result[flag].col+"---"+this.ExamEndSlot[i]);
    //           if(MaxSeatBooked<Number(result[flag].A))
    //           {
    //             MaxSeatBooked=Number(result[flag].A);
    //           }
    //        }
    //     }
    //     console.log(this.ExamStartSlot[i]+"--"+this.ExamEndSlot[i]+"="+MaxSeatBooked);
    //     let SlotAval=Number(this.Seats)-MaxSeatBooked;
    //     console.log(SlotAval+"---"+this.Seats);
    //     if(SlotAval<0)
    //     {
    //       SlotAval=0;
    //     }

    //     this.ExamTimeSlot.push({StartTime:this.ExamStartSlot[i],EndTime:this.ExamEndSlot[i],Seat:SlotAval});
    //     //    flag_one++;
    //     // console.log(this.ExamStartSlot[i]+"--"+this.ExamEndSlot[i]);
    //     // console.log("------");

    //   }
    //   // if("13:50"<"14:50")
    //   // {
    //   //   console.log("true");
    //   // }
    //   // else{
    //   //   console.log("false");
    //   // }

    let flag = 0;
    let MaxSeatBooked = 0;
    // // console.log(result); 
    //  console.log(this.ExamStartSlot);
    for (let i = 0; i < this.ExamStartSlot.length; i++) {
      flag = 0;
      MaxSeatBooked = 0;

      // console.log(result[flag].col.replace(":","")+"---"+"---"+this.ExamStartSlot[i].replace(new RegExp('^0+(?!$)',''),'').replace(":","")+"---"+this.ExamEndSlot[i].replace(new RegExp('^0+(?!$)',''),'').replace(":",""));
      while (Number(result[flag].col.replace(":", "")) < Number(this.ExamEndSlot[i].replace(new RegExp('^0+(?!$)', ''), '').replace(":", ""))) {
        // console.log("In While");
        Number(result[flag].col.replace(":", "")) + "---" + Number(this.ExamEndSlot[i].replace(new RegExp('^0+(?!$)', ''), '').replace(":", ""))
        if (Number(result[flag].col.replace(":", "")) >= Number(this.ExamStartSlot[i].replace(new RegExp('^0+(?!$)', ''), '').replace(":", ""))) {

          if (MaxSeatBooked < Number(result[flag].A)) {
            MaxSeatBooked = Number(result[flag].A);
            console.log(MaxSeatBooked);
          }
        }
        flag++;
      }
      flag++;
      console.log(this.ExamStartSlot[i] + "--" + this.ExamEndSlot[i] + "=" + MaxSeatBooked);
      let SlotAval = Number(this.Seats) - MaxSeatBooked;
      //   console.log(SlotAval+"---"+this.Seats);
      if (SlotAval < 0) {
        SlotAval = 0;
      }
      this.ExamTimeSlot.push({ StartTime: this.ExamStartSlot[i], EndTime: this.ExamEndSlot[i], Seat: SlotAval });


    }
  }
  CheckSlot(StartTime, EndTime) {
    // console.log(this.NExamStartSlot);
    // console.log(this.NExamEndSlot);
    // for(let i=0;i<this.NExamStartSlot.length;i++)
    // {
    //   console.log(this.NExamStartSlot[i]+"----"+StartTime+"---"+EndTime);
    //     if(this.NExamStartSlot[i]==StartTime||this.NExamStartSlot[i]==EndTime)
    //     {
    //       console.log("YES");
    //       return 0;

    //     }

    // }
    // return 1;
    let StartTimeArr = StartTime.split(":");
    let endTimeArr = EndTime.split(":");
    //   console.log(StartTimeArr[0]+"---"+endTimeArr[0]);

    if (StartTimeArr[0] <= 11 && endTimeArr[0] >= 12) {
      return 0;
    }
    if (StartTime == "13:00" || StartTime == "13:15") {
      return 0;
    }
    for (let i = 0; i < this.NExamStartSlot.length; i++) {
      //   console.log(this.NExamStartSlot[i]+"----"+StartTime+"---"+EndTime);
      if (this.NExamStartSlot[i] == StartTime || this.NExamStartSlot[i] == EndTime) {

        console.log("YES");
        return 0;

      }


    }
    return 1;

  }
  TimeData(result: any) {
    let flag_one = 0;
    this.ExamTimeSlot.length = 0;
    for (let i = 0; i < result.length; i++) {
      //  console.log(result);
      flag_one = 0;

      for (let j = flag_one; j < this.ExamStartSlot.length; j++) {

        if (this.ExamStartSlot[j] == result[i].StartTime || this.ExamEndSlot[j] == result[i].EndTime) {
          this.ExamTimeSlot.push({ StartTime: this.ExamStartSlot[j], EndTime: this.ExamEndSlot[j], Seat: result[i].Seats });
          flag_one++;
          break;

        }
        else {
          //  console.log(this.ExamStartSlot[j]+"="+result[i].StartTime);
          this.ExamTimeSlot.push({ StartTime: this.ExamStartSlot[j], EndTime: this.ExamEndSlot[j], Seat: this.Seats });
          flag_one++;
        }
      }

    }
    for (let i = flag_one; i < this.ExamStartSlot.length; i++) {
      this.ExamTimeSlot.push({ StartTime: this.ExamStartSlot[i], EndTime: this.ExamEndSlot[i], Seat: this.Seats });
    }
  }
  GetTotalSeats() {
    this.http.get("http://wiu.edu/CITR/ExamSchedule/SaveSeats.sphp?seats=0&campus=" + this.CampusName)
      .subscribe((data) => {
        this.Seats = data;

      }, (error) => {

        console.log(error);

      });
  }
  getSeatColor(seatCount: any) {
    if (seatCount > 10) {
      return "#d1efae";
    }
    if (seatCount <= 10 && seatCount > 5) {
      return "#fff7b1";
    }
    if (seatCount <= 5) {
      return "#ffc7b687";
    }
  }


  BookStudentExamSlot(StartTime: any, EndTime: any, selectedDate: any) {

    let Note = this.note.replace(new RegExp('\n', 'g'), '\\n');
    console.log("http://wiu.edu/CITR/ExamSchedule/saveNonWiuStudentReservation.sphp?c_name=" + this.c_name + "&s_name=" + this.s_name + "&s_id=" + this.s_id + "&s_email=" + this.s_email
      + "&s_phone=" + this.s_phone + "&note=" + Note + "&examTime=" + this.ExamTime + "&examDate=" + this.SelectedDate + "&startTime=" + StartTime + "&endTime=" + EndTime);

    this.http.get("http://wiu.edu/CITR/ExamSchedule/saveNonWiuStudentReservation.sphp?c_name=" + this.c_name + "&s_name=" + this.s_name + "&s_id=" + this.s_id + "&s_email=" + this.s_email
      + "&s_phone=" + this.s_phone + "&note=" + Note + "&examTime=" + this.ExamTime + "&examDate=" + this.SelectedDate + "&startTime=" + StartTime + "&endTime=" + EndTime + "&examType=" + this.examType)
      .subscribe((data) => {
        this.openSnackBar('Exam Booked!!!', "OK");

      }, (error) => {

        console.log(error);
        this.openSnackBar('Exam Booked!!!', "OK");
      });

  }

  ManualTimeSave(SelectedDate: any) {
    console.log(this.startTime_Manual + " To " + this.endTime_Manual);
    this.BookStudentExamSlot(this.startTime_Manual, this.endTime_Manual, SelectedDate);
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }
}
