import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookExamComponent } from './book-exam.component';

describe('BookExamComponent', () => {
  let component: BookExamComponent;
  let fixture: ComponentFixture<BookExamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookExamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
