import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditExamScheduleComponent } from './edit-exam-schedule.component';

describe('EditExamScheduleComponent', () => {
  let component: EditExamScheduleComponent;
  let fixture: ComponentFixture<EditExamScheduleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditExamScheduleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditExamScheduleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
