import { Component, QueryList, ViewChildren } from "@angular/core";
import {
  HttpClient,
  HttpEvent,
  HttpErrorResponse,
  HttpEventType
} from "@angular/common/http";
import { OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { getLocaleMonthNames } from "@angular/common";
import { MatStepperModule, MatStepper } from "@angular/material/stepper";
import { ActivatedRoute, Router } from "@angular/router";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  @ViewChildren("examTime") examTime: QueryList<any>;
  @ViewChildren("types") types: QueryList<any>;
  @ViewChildren("stepper") stepper: MatStepper;

  title = "ExamSchedule";
  starNumber: any;
  courseDetails: any = [];
  isLinear = false;
  NumberOfExam: any;
  NumberOfExamAray: any = [];
  startDate: any = [];
  endDate: any = [];
  ExamTime: any;
  Month_1: any;
  isEditable: any;
  //title = 'ExamScheduleMobile';
  wiuId: any;
  sKey: any;
  url: any;
  studentEmail: any;
  studentName: any;
  constructor(
    public http: HttpClient,
    private router: Router,
    private route: ActivatedRoute
  ) {
    // this.router.navigate(['viewReservation']).then(result=>{
    //   window.open('_blank');
    // })
    this.starNumber = "57504";
    this.isEditable = "true";

    this.studentEmail = sessionStorage.getItem("semail");
    this.studentName = sessionStorage.getItem("sname");
    this.wiuId = sessionStorage.getItem("wiuId");
    this.sKey = sessionStorage.getItem("key");
    console.log(this.wiuId);
    if (this.wiuId != null) {
      //   this.UserWIULogin();
      console.log("FOUND");
    } else {
      //    this.GetInfo();
      console.log("Not FOUND");
      this.GetInfo();
    }
  }
  GetInfo() {
    //   this.route.queryParams.subscribe(params => {
    //     this.wiuId = params['wiuId'];
    //     this.sKey = params['key'];

    this.url = window.location.href;

    // });
    console.log(this.url);

    this.SendData(this.url);
  }
  SendData(url: any) {
    let para = url.split("?");
    // this.wiuId=sessionStorage.getItem('wiuId');
    // this.sKey=sessionStorage.getItem('key');

    if (this.sKey) {
      //this.router.navigateByUrl('/home');

      let para2 = para[1].split("&");
      let para1 = para[1].split("&");
      let wiuId = para1[0].split("=");
      let key = para1[1].split("=");
      console.log(wiuId[1]);
      console.log(key[1]);

      //  sessionStorage.setItem("wiuId",wiuId[1]);
      //  sessionStorage.setItem("key",key[1]);
      this.UserWIULogin();
    } else if (!para[1]) {
      sessionStorage.removeItem("wiuId");
      sessionStorage.removeItem("key");
      // window.location.href='https://wiu.edu/CITR/MyWestern/ExamSchedule/adminLogin.sphp';
    } else {
      let para2 = para[1].split("&");
      let para1 = para[1].split("&");
      let wiuId = para1[0].split("=");
      let key = para1[1].split("=");
      console.log(wiuId[1]);
      console.log(key[1]);

      sessionStorage.setItem("wiuId", wiuId[1]);
      sessionStorage.setItem("key", key[1]);
      this.UserWIULogin();
      //this.router.navigateByUrl('/home');
    }
  }
  UserWIULogin() {
    this.http
      .get(
        "http://wiu.edu/CITR/ExamSchedule/MobileWeb/checkLogin.sphp?wiuId=" +
          this.wiuId +
          "&key=" +
          this.sKey
      )
      .subscribe(
        data => {
          console.log(data);
          //  sessionStorage.setItem("wiuLogin",this.wiuId);
          if (data != 1) {
            console.log(data);
            //  window.location.href='https://wiu.edu/CITR/MyWestern/ExamSchedule/adminLogin.sphp';
          } else {
            // this.SearchStudent(this.wiuId);
            sessionStorage.setItem("wiuId", this.wiuId);
            sessionStorage.setItem("key", this.sKey);
          }
        },
        error => {
          console.log(error);
        }
      );
  }

  nextClicked(event) {
    // complete the current step
  }

  SaveExam(
    ExamTime: any,
    StartDate: any,
    EndDate: any,
    ExamType: any,
    Note: any,
    stepper: MatStepper
  ) {
    // console.log(item);
    // let examTime = this.examTime.toArray();
    // this.ExamTime=examTime[0].nativeElement.innerHTML;

    // move to next step

    this.isEditable = "false";

    if (ExamTime != "" && StartDate != "" && EndDate != "" && ExamType != "") {
      this.startDate = StartDate.toString().split(" ", 4);
      this.Month_1 = this.getMonth();
      StartDate =
        this.startDate[3] + "/" + this.startDate[2] + "/" + this.Month_1;
      console.log(StartDate);
      this.endDate = EndDate.toString().split(" ", 4);
      this.Month_1 = this.getMonth();
      EndDate = this.endDate[3] + "/" + this.endDate[2] + "/" + this.Month_1;
      console.log(EndDate);
      console.log(ExamType);

      console.log(
        "http://wiu.edu/CITR/ExamSchedule/saveExam.sphp?starNumber=" +
          this.starNumber +
          "&startDate=" +
          StartDate +
          "&endDate=" +
          EndDate +
          "&examType=" +
          ExamType +
          "&examTime=" +
          ExamTime +
          "&note=" +
          Note +
          "&courseTitle=" +
          this.courseDetails[0].title +
          "&section=" +
          this.courseDetails[0].section +
          "&term=" +
          this.courseDetails[0].term +
          "&instrutor=" +
          this.courseDetails[0].instrutor +
          "&dept=" +
          this.courseDetails[0].department
      );
      this.http
        .get(
          "http://wiu.edu/CITR/ExamSchedule/saveExam.sphp?starNumber=" +
            this.starNumber +
            "&startDate=" +
            StartDate +
            "&endDate=" +
            EndDate +
            "&examType=" +
            ExamType +
            "&examTime=" +
            ExamTime +
            "&note=" +
            Note +
            "&courseTitle=" +
            this.courseDetails[0].title +
            "&section=" +
            this.courseDetails[0].section +
            "&term=" +
            this.courseDetails[0].term +
            "&instrutor=" +
            this.courseDetails[0].instrutor +
            "&dept=" +
            this.courseDetails[0].department
        )
        .subscribe(
          data => {
            console.log(data[0].title);
            this.courseDetails = data;
          },
          error => {
            console.log(error);
          }
        );
      stepper.next();
    } else {
      console.log("Enter All Field");
    }
  }
  SearchCourse() {
    console.log(this.starNumber);
    this.http
      .get(
        "http://wiu.edu/CITR/ExamSchedule/getCourseDetails.sphp?starNumber=" +
          this.starNumber
      )
      .subscribe(
        data => {
          console.log(data[0].title);
          this.courseDetails = data;
        },
        error => {
          console.log(error);
        }
      );
  }

  CreateExamSlot() {
    for (let i = 0; i < this.NumberOfExam; i++) {
      this.NumberOfExamAray[i] = i + 1;
    }
  }

  getMonth() {
    if (this.startDate[1] == "Jan" || this.endDate[1] == "Jan") {
      this.Month_1 = "01";
    }
    if (this.startDate[1] == "Feb" || this.endDate[1] == "Jan") {
      this.Month_1 = "02";
    }

    if (this.startDate[1] == "May" || this.endDate[1] == "Jan") {
      this.Month_1 = "05";
    }
    if (this.startDate[1] == "Mar" || this.endDate[1] == "Jan") {
      this.Month_1 = "03";
    }
    if (this.startDate[1] == "Apr" || this.endDate[1] == "Jan") {
      this.Month_1 = "04";
    }
    if (this.startDate[1] == "Jun" || this.endDate[1] == "Jan") {
      this.Month_1 = "06";
    }
    if (this.startDate[1] == "Jul" || this.endDate[1] == "Jan") {
      this.Month_1 = "07";
    }
    if (this.startDate[1] == "Aug" || this.endDate[1] == "Jan") {
      this.Month_1 = "08";
    }
    if (this.startDate[1] == "Sep" || this.endDate[1] == "Jan") {
      this.Month_1 = "09";
    }
    if (this.startDate[1] == "Oct" || this.endDate[1] == "Jan") {
      this.Month_1 = "10";
    }
    if (this.startDate[1] == "Nov" || this.endDate[1] == "Jan") {
      this.Month_1 = "11";
    }
    if (this.startDate[1] == "Dec" || this.endDate[1] == "Jan") {
      this.Month_1 = "12";
    }
    return this.Month_1;
  }
}
