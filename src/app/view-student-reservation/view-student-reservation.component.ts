import { Component, OnInit, ViewChild } from '@angular/core';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { StudentSignComponent } from '../student-sign/student-sign.component';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-view-student-reservation',
  templateUrl: './view-student-reservation.component.html',
  styleUrls: ['./view-student-reservation.component.css']
})
export class ViewStudentReservationComponent implements OnInit {
  DateIs_: any;
  flag: any = 0;
  examReservationData: any = [];
  StartTime_Temp: any;
  EndTime_Temp: any;
  Picker: any;
  picker: any;
  checkIn: any;
  wiuId: any = '918636544';
  deleteId: any;
  CheckInHr: any;
  CheckInMin: any;
  CheckOutHr: any;
  CheckOutMin: any;
  leftMin: any;
  Sid: any;
  email: any;
  lastName: any;
  find: any;
  @ViewChild(SignaturePad) signaturePad: SignaturePad;

  public signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    // 'minWidth': 2,
    'canvasWidth': 400,
    'canvasHeight': 400,
    minDistance: 2,
    velocityFilterWeight: 0
  };
  constructor(private http: HttpClient, public dialog: MatDialog) {
    this.flag = 0;
    this.picker = new Date((new Date().getTime() - 3888000000));

    // console.log(this.picker);
  }

  ngOnInit() {
  }
  getExamReservation(email: any, lastName: any, wiuId: any, find: any) {
    //this.DateIs_=dateIs;
    //  console.log(lastName);
    this.find = find;
    console.log(find);
    let url = "";
    if (this.find == 1) {
      lastName = '';
      url = "http://wiu.edu/CITR/ExamSchedule/getExamReservation.sphp?email=" + email + "&lastName=" + lastName;

    }
    else if (find == 2) {
      email = '';
      url = "http://wiu.edu/CITR/ExamSchedule/getExamReservation.sphp?email=" + email + "&lastName=" + lastName;

    }

    if (find == 3) {
      url = "http://wiu.edu/CITR/ExamSchedule/getExamReservation.sphp?wiuId=" + wiuId;

    }
    //this.examReservationData.length=0;
    this.email = email;
    this.lastName = lastName;
    // let DateIs=new Date(dateIs);
    //  let DateMonth=Number(DateIs.getMonth())+1
    // let Date_=DateIs.getFullYear()+"/"+DateMonth+"/"+DateIs.getDate();
    //  this.DateIs_=Date_;
    console.log(url)
    this.http.get(url)
      .subscribe((data) => {
        this.examReservationData = data;
        //   console.log(data);
        this.flag = 1;

      }, (error) => {

        console.log(error);

      });
  }

  ShowData() {
    return this.flag;
  }
  SetSlot(StartTime: any, EndTime: any) {
    console.log(this.examReservationData.length);
    if (this.examReservationData.length == 1) {

      return 1;
    }
    if (this.StartTime_Temp == StartTime && this.EndTime_Temp == EndTime) {
      return 0;
    }
    else {
      this.StartTime_Temp = StartTime;
      this.EndTime_Temp = EndTime;
      return 1;
    }

  }
  StudentEntry(isCheck: any, sid: any, status: any) {
    let TodayIs = new Date();
    let CheckIn = String("0" + TodayIs.getHours()).slice(-2) + ":" + String("0" + TodayIs.getMinutes()).slice(-2);
    //  console.log(isCheck+"---"+status);
    if (isCheck == 'uncheck') {
      if (status == 'checkIn') {
        //    console.log("http://wiu.edu/CITR/ExamSchedule/StudentCheckInCheckOut.sphp?sid="+sid+"&unCheck=true&checkIn=NULL");
        this.http.get("http://wiu.edu/CITR/ExamSchedule/StudentCheckInCheckOut.sphp?sid=" + sid + "&unCheck=true&checkIn=NULL")
          .subscribe((data) => {
            // this.examReservationData=data;
            // console.log(data);
            // this.flag=1;
            this.getExamReservation(this.email, this.lastName, this.wiuId, this.find);
          }, (error) => {

            console.log(error);

          });
      }
      else {
        this.http.get("http://wiu.edu/CITR/ExamSchedule/StudentCheckInCheckOut.sphp?sid=" + sid + "&unCheck=true&checkOut=NULL")
          .subscribe((data) => {
            // this.examReservationData=data;
            // console.log(data);
            // this.flag=1;
            this.getExamReservation(this.email, this.lastName, this.wiuId, this.find);
          }, (error) => {

            console.log(error);

          });
      }
    }
    else {
      if (status == 'checkIn') {
        //     console.log("http://wiu.edu/CITR/ExamSchedule/StudentCheckInCheckOut.sphp?sid="+sid+"&unCheck=false&checkIn="+CheckIn);
        this.http.get("http://wiu.edu/CITR/ExamSchedule/StudentCheckInCheckOut.sphp?sid=" + sid + "&unCheck=false&checkIn=" + CheckIn)
          .subscribe((data) => {
            // this.examReservationData=data;
            // console.log(data);
            // this.flag=1;
            this.getExamReservation(this.email, this.lastName, this.wiuId, this.find);
          }, (error) => {

            console.log(error);

          });
      }
      else {
        this.http.get("http://wiu.edu/CITR/ExamSchedule/StudentCheckInCheckOut.sphp?sid=" + sid + "&unCheck=false&checkOut=" + CheckIn)
          .subscribe((data) => {
            // this.examReservationData=data;
            // console.log(data);
            // this.flag=1;
            //  this.getExamReservation();
            this.OpenSignaturePopup(sid);
            this.getExamReservation(this.email, this.lastName, this.wiuId, this.find);
          }, (error) => {

            console.log(error);

          });
      }

    }


  }

  OpenSignaturePopup(sid: any): void {
    const dialogRef = this.dialog.open(StudentSignComponent, {
      width: '450px',
      data: { sid: sid },
      backdropClass: "cdk-overlay-dark-backdrop"

    });

    dialogRef.afterClosed().subscribe(result => {
      //  console.log('The dialog was closed');

    });


  }

  EditCheckIn(sid: any) {
    this.Sid = sid;
  }
  EditCheckInPopUpConfirm() {
    // console.log(this.CheckInHr+":"+this.CheckInMin);
    this.CheckInMin = String("0" + this.CheckInMin).slice(-2)
    // console.log("http://wiu.edu/CITR/ExamSchedule/StudentCheckInCheckOut.sphp?sid="+this.Sid+"&unCheck=false&checkIn="+this.CheckInHr+":"+this.CheckInMin)
    this.http.get("http://wiu.edu/CITR/ExamSchedule/StudentCheckInCheckOut.sphp?sid=" + this.Sid + "&unCheck=false&checkIn=" + this.CheckInHr + ":" + this.CheckInMin)
      .subscribe((data) => {
        // this.examReservationData=data;
        // console.log(data);
        // this.flag=1;
        this.getExamReservation(this.email, this.lastName, this.wiuId, this.find);
      }, (error) => {

        console.log(error);

      });
  }

  EditCheckOut(sid: any) {
    this.Sid = sid;
  }
  EditCheckOutPopUpConfirm() {
    // console.log(this.CheckInHr+":"+this.CheckInMin);
    this.CheckOutMin = String("0" + this.CheckOutMin).slice(-2)
    //  console.log("http://wiu.edu/CITR/ExamSchedule/StudentCheckInCheckOut.sphp?sid="+this.Sid+"&unCheck=false&checkOut="+this.CheckOutHr+":"+this.CheckOutMin)
    this.http.get("http://wiu.edu/CITR/ExamSchedule/StudentCheckInCheckOut.sphp?sid=" + this.Sid + "&unCheck=false&checkOut=" + this.CheckOutHr + ":" + this.CheckOutMin)
      .subscribe((data) => {
        // this.examReservationData=data;
        // console.log(data);
        // this.flag=1;
        this.getExamReservation(this.email, this.lastName, this.wiuId, this.find);
      }, (error) => {

        console.log(error);

      });
  }

  UpdateCheckStatus(sid: any, isCheck: any, status: any) {
    this.http.get("http://wiu.edu/CITR/ExamSchedule/StudentCheckInCheckOut.sphp?sid=" + sid + "&unCheck=true&checkIn=NULL")
      .subscribe((data) => {
        // this.examReservationData=data;
        // console.log(data);
        // this.flag=1;

      }, (error) => {

        console.log(error);

      });
  }

  DeleteExamSchedule(id: any) {
    //  console.log(id);
    this.deleteId = id;
  }
  DeleteExamSchedule_Popup() {
    this.http.get("http://wiu.edu/CITR/ExamSchedule/deleteStudentExamSchedule.sphp?id=" + this.deleteId)
      .subscribe((data) => {
        this.getExamReservation(this.email, this.lastName, this.wiuId, this.find);

      }, (error) => {

        console.log(error);

      });
  }

}
