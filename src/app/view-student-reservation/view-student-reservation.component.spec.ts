import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewStudentReservationComponent } from './view-student-reservation.component';

describe('ViewStudentReservationComponent', () => {
  let component: ViewStudentReservationComponent;
  let fixture: ComponentFixture<ViewStudentReservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewStudentReservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewStudentReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
