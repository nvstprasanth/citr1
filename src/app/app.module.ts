import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatCardModule } from "@angular/material/card";
import { FormsModule } from "@angular/forms";

import { HttpClientModule } from "@angular/common/http";
import { MatProgressBarModule } from "@angular/material/progress-bar";
import { MatStepperModule } from "@angular/material/stepper";
import { MatSelectModule } from "@angular/material/select";
import { MatDatepickerModule } from "@angular/material/datepicker";
import {
  MatCheckboxModule,
  MatRadioModule,
  MatTableModule,
  MatNativeDateModule
} from "@angular/material";
import { MatListModule } from "@angular/material/list";
import { RouterModule } from "@angular/router";
import { BookExamComponent } from "./book-exam/book-exam.component";
import { AppRoutingModule } from "./app-routing.module";
import { ScheduleExamComponent } from "./schedule-exam/schedule-exam.component";
import { MatGridListModule } from "@angular/material/grid-list";
import {
  MatDialogModule,
  MAT_DIALOG_DEFAULT_OPTIONS
} from "@angular/material/dialog";
import { BookExamPopUpComponent } from "./book-exam-pop-up/book-exam-pop-up.component";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatRippleModule } from "@angular/material/core";
import { ViewReservationComponent } from "./view-reservation/view-reservation.component";
import { ViewStudentReservationComponent } from "./view-student-reservation/view-student-reservation.component";
import { SignaturePadModule } from "angular2-signaturepad";
import { StudentSignComponent } from "./student-sign/student-sign.component";
import { LZStringModule, LZStringService } from "ng-lz-string";
import { SettingsComponent } from "./settings/settings.component";
import { AmazingTimePickerModule } from "amazing-time-picker";
import { MatTooltipModule } from "@angular/material/tooltip";
import { CountdownModule } from "ngx-countdown";
import { EditExamScheduleComponent } from "./edit-exam-schedule/edit-exam-schedule.component";
import { ToastrModule } from "ngx-toastr";
import { HashLocationStrategy, LocationStrategy } from "@angular/common";
import { StudentDetailsComponent } from "./student-details/student-details.component";
import { NgxSpinnerModule } from "ngx-spinner";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { MatSlideToggleModule } from "@angular/material/slide-toggle";
import { NonWIUStudentBookSlotComponent } from "./non-wiustudent-book-slot/non-wiustudent-book-slot.component";
import { TimeGraphComponent } from "./time-graph/time-graph.component";
import { NotesComponent } from "./notes/notes.component";
@NgModule({
  declarations: [
    AppComponent,
    BookExamComponent,
    ScheduleExamComponent,
    BookExamPopUpComponent,
    ViewReservationComponent,
    ViewStudentReservationComponent,
    StudentSignComponent,
    SettingsComponent,
    EditExamScheduleComponent,
    StudentDetailsComponent,
    NonWIUStudentBookSlotComponent,
    TimeGraphComponent,
    NotesComponent
  ],
  imports: [
    BrowserModule,
    RouterModule,
    MatCheckboxModule,
    BrowserAnimationsModule,
    MatRippleModule,
    SignaturePadModule,
    MatSidenavModule,
    MatTableModule,
    MatGridListModule,
    AmazingTimePickerModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule,
    NgxSpinnerModule,
    MatToolbarModule,
    MatSelectModule,
    MatSnackBarModule,
    CountdownModule,
    MatButtonToggleModule,
    MatSlideToggleModule,
    MatIconModule,
    FormsModule,
    MatListModule,
    MatDialogModule,
    LZStringModule,
    MatCardModule,
    MatStepperModule,
    MatRadioModule,
    MatNativeDateModule,
    HttpClientModule,
    MatProgressBarModule,
    MatDatepickerModule,
    ToastrModule.forRoot(),
    AppRoutingModule
  ],
  providers: [
    LZStringService,
    { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: true } },
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
