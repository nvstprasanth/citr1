import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-time-graph',
  templateUrl: './time-graph.component.html',
  styleUrls: ['./time-graph.component.css']
})
export class TimeGraphComponent implements OnInit {

  StudentTimeData: any = [];
  SlotAvailable: any = [];
  DateToSearch: any;
  constructor(public http: HttpClient, public dialogRef: MatDialogRef<TimeGraphComponent>,
    @Inject(MAT_DIALOG_DATA) public impdata: any) {
    this.DateToSearch = impdata.dateToSearch;
    console.log(this.DateToSearch);
    this.SaveLunchTime();
    this.GetSlotAvailable();
  }

  ngOnInit() {
  }

  SaveLunchTime() {
    Swal.showLoading();
    //   console.log("http://wiu.edu/CITR/ExamSchedule/getSaveLunchTime.sphp?startTime="+this.L_StartTime+"&endTime="+this.L_EndTime+"&read=false");
    this.http.get("http://wiu.edu/CITR/ExamSchedule/GetTimeSlotDetails.php?examDate=" + this.DateToSearch)
      .subscribe((data) => {
        //this.Oldseats=data;
        this.StudentTimeData = data;
        console.log(data);
        // Swal.hideLoading();
        Swal.close();
      }, (error) => {

        console.log(error);

      });
  }



  GetSlotAvailable() {
    //   console.log("http://wiu.edu/CITR/ExamSchedule/getSaveLunchTime.sphp?startTime="+this.L_StartTime+"&endTime="+this.L_EndTime+"&read=false");
    this.http.get("http://wiu.edu/CITR/ExamSchedule/GetSlot.php?examDate=" + this.DateToSearch + "&campus=macomb")
      .subscribe((data) => {
        //this.Oldseats=data;
        this.SlotAvailable = data;
        console.log(data);

      }, (error) => {

        console.log(error);

      });
  }
}
