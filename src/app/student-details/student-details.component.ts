import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { NgxSpinnerService } from "ngx-spinner";
import { MatSnackBar } from '@angular/material/snack-bar';
@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.css']
})
export class StudentDetailsComponent implements OnInit {

  sname = 'Rahul';
  email;
  show = 0;
  StudentDetails: any = [];
  constructor(private http: HttpClient, private _snackBar: MatSnackBar, private spinner: NgxSpinnerService) { }

  ngOnInit() {

  }

  getBgColor(i: any) {
    // console.log(i);
    if (i % 2 == 0) {
      return '#eaeaea';
    }
  }

  getStudentDetails() {

    this.spinner.show();
    this.http.get("http://wiu.edu/CITR/ExamSchedule/GetOnlyStudentDetails.sphp?sname=" + this.sname)
      .subscribe((data) => {
        console.log(data);
        this.StudentDetails = data;
        this.spinner.hide();
        this.show = 1;
      }, (error) => {

        console.log(error);

      });
  }

  // ShowData()
  // {
  //     return this.flag;
  // }
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
  copyData(info: any) {
    let selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = info;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
    this.openSnackBar('Copy Infomation', 'OK');
  }
}
