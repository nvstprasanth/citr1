import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Routes } from "@angular/router";
import { BookExamComponent } from "./book-exam/book-exam.component";
import { ScheduleExamComponent } from "./schedule-exam/schedule-exam.component";
import { AppComponent } from "./app.component";
import { BookExamPopUpComponent } from "./book-exam-pop-up/book-exam-pop-up.component";
import { ViewReservationComponent } from "./view-reservation/view-reservation.component";
import { ViewStudentReservationComponent } from "./view-student-reservation/view-student-reservation.component";
import { StudentSignComponent } from "./student-sign/student-sign.component";
import { SettingsComponent } from "./settings/settings.component";
import { EditExamScheduleComponent } from "./edit-exam-schedule/edit-exam-schedule.component";
import { StudentDetailsComponent } from "./student-details/student-details.component";
import { NonWIUStudentBookSlotComponent } from "./non-wiustudent-book-slot/non-wiustudent-book-slot.component";
import { TimeGraphComponent } from "./time-graph/time-graph.component";
import { NotesComponent } from "./notes/notes.component";

const appRoutes: Routes = [
  // { path: '',
  //   component: ViewReservationComponent,
  //   pathMatch:'full'
  // },
  { path: "home", component: ScheduleExamComponent },
  {
    path: "bookExam",
    component: BookExamComponent
  },

  {
    path: "bookExamPopUp",
    component: BookExamPopUpComponent
  },
  {
    path: "viewReservation",
    component: ViewReservationComponent
  },
  {
    path: "viewStudentReservation",
    component: ViewStudentReservationComponent
  },
  {
    path: "studentSign",
    component: StudentSignComponent
  },
  {
    path: "settings",
    component: SettingsComponent
  },
  {
    path: "editexam",
    component: EditExamScheduleComponent
  },
  {
    path: "studentDetails",
    component: StudentDetailsComponent
  },
  {
    path: "nonWiuStudent",
    component: NonWIUStudentBookSlotComponent
  },
  {
    path: "graph",
    component: TimeGraphComponent
  },
  {
    path: "notes",
    component: NotesComponent
  }
];

@NgModule({
  declarations: [],
  exports: [RouterModule],
  imports: [RouterModule.forRoot(appRoutes), CommonModule]
})
export class AppRoutingModule {}
