import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentSignComponent } from './student-sign.component';

describe('StudentSignComponent', () => {
  let component: StudentSignComponent;
  let fixture: ComponentFixture<StudentSignComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentSignComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentSignComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
